accounts = {
    'opalstack-<mailbox>': '<password>',
    'webfaction-<mailbox>': '<password>',
}

hosts = {'opalstack': 'mail.us.opalstack.com',
         'webfaction': 'mail.webfaction.com'}

mbsyncrc = '# ACCOUNTS\n'

for account in accounts:
    provider, user = account.split('-')
    mbsyncrc += f'''
IMAPAccount {provider}_{user}
SSLType IMAPS
Host {hosts[provider]}
User {user}
Pass {accounts[account]}
'''

mbsyncrc += '\n# STORES\n'

for account in accounts:
    provider, user = account.split('-')
    mbsyncrc += f'''
IMAPStore {provider}_{user}
Account {provider}_{user}
'''

mbsyncrc += '\n# CHANNELS\n'

for account in accounts:
    provider, user = account.split('-')
    if provider == 'webfaction':
        continue
    mbsyncrc += f'''
Channel {user}
Master :webfaction_{user}:
Slave :opalstack_{user}:
Patterns *
Create Slave
Sync Pull
'''

print(mbsyncrc)

with open('mbsyncrc', 'w+') as mbsyncrc_f:
    mbsyncrc_f.write(mbsyncrc)
